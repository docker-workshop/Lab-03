# Docker Workshop
Lab 03: Running commands inside the container

---


## Instructions

 - Run the application in a Docker container (detached mode) using:
```
docker run -d -p 3000:3000 --name app selaworkshops/npm-static-app:latest
```

 - Ensure the container is running:
```
docker ps
```

 - Attach to the container process using:
```
docker attach app
```

 - Note that you are attached to the server process, not to a terminal inside the container
 - You are not stuck, this is fine
 - Exit from the process by clicking on the command below couple of times:
```
(CTRL + C)
```

 - Check the status running containers:
```
docker ps
```

 - Show the stopped containers as well:
```
docker ps -a
```

 - If the container was stoped, delete it and run a new container using:
```
docker rm app
docker run -d -p 3000:3000 --name app selaworkshops/npm-static-app:latest
```

 - Execute the terminal (interactive) inside the container using:
```
docker exec -it app /bin/bash
```

 - Inspect the container's filesystem: ("#" sign here indicates that you are inside the container)
```
ls -l
pwd
cd /
ls -l
```

 - Exit from the container terminal using:
```
exit
```

 - Check the running containers:
```
docker ps
```

 - Remove the "app" container:
```
docker rm -f app
```
